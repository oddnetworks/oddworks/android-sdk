package io.oddworks.device.authentication

import android.accounts.Account
import android.accounts.AccountAuthenticatorActivity
import android.accounts.AccountManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.KeyEvent
import android.widget.TextView
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethod
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import io.oddworks.device.R
import io.oddworks.device.exception.BadResponseCodeException
import io.oddworks.device.model.OddViewer
import io.oddworks.device.model.common.OddResourceType
import io.oddworks.device.request.OddRequest
import io.oddworks.device.request.RxOddCall
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

open class OddAuthenticationActivity : AccountAuthenticatorActivity() {
    protected lateinit var authTokenType: String
    protected lateinit var accountManager: AccountManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.OddAuthenticationTheme)
        setContentView(R.layout.activity_odd_authentication)
        accountManager = AccountManager.get(baseContext)

        val accountEmail = intent.getStringExtra(ARG_ACCOUNT_EMAIL)
        authTokenType = intent.getStringExtra(ARG_AUTH_TYPE) ?: OddAuthenticator.AUTH_TOKEN_TYPE_ODDWORKS_DEVICE

        if (accountEmail != null) {
            findViewById<TextView>(R.id.odd_authentication_email).text = accountEmail
        }
        findViewById<EditText>(R.id.odd_authentication_password).setOnEditorActionListener { textView, actionId, _ ->
            if (actionId == R.integer.authImeActionId || actionId == EditorInfo.IME_NULL || actionId == EditorInfo.IME_ACTION_DONE) {
                val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

                inputManager.hideSoftInputFromWindow(textView.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                submit(textView)

                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
        val authLogoIdentifier = resources.getIdentifier("oddworks_authentication_logo", "drawable", packageName)
        if (authLogoIdentifier > 0) {
            findViewById<ImageView>(R.id.odd_authentication_logo).let {
                it.visibility = View.VISIBLE
                it.setImageDrawable(ContextCompat.getDrawable(this, authLogoIdentifier))
                it.requestLayout()
            }
        }
    }

    fun submit(view: View) {
        val email = findViewById<TextView>(R.id.odd_authentication_email).text.toString()
        val password = findViewById<TextView>(R.id.odd_authentication_password).text.toString()
        val accountType = intent.getStringExtra(ARG_ACCOUNT_TYPE)

        hideFailure()
        showProgress()

        RxOddCall
                .observableFrom<OddViewer> {
                    Log.d(TAG, "Started authenticating - $authTokenType -  $email")
                    OddRequest.Builder(baseContext, OddResourceType.VIEWER)
                            .login(email, password)
                            .build()
                            .enqueueRequest(it)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.d(TAG, "login successful - $authTokenType -  $email")

                    finishLogin(accountType, it)
                },{
                    Log.d(TAG, "login failed - $authTokenType -  $email", it)
                    val message = when (it) {
                        is BadResponseCodeException -> {
                            var detail = ""
                            if (it.oddErrors.isNotEmpty()) {
                                val error = it.oddErrors.first()
                                detail = if (error.detail.isNullOrBlank()) {
                                    error.title
                                } else {
                                    error.detail
                                }
                            }

                            getString(R.string.oddworks_authentication_failed_with_detail, detail)
                        }
                        else -> {
                            getString(R.string.oddworks_authentication_failed_no_detail)
                        }
                    }
                    setFailureMessage(message)
                    showFailure()
                    hideProgress()
                })
    }

    protected fun showProgress() {
        val button = findViewById<View>(R.id.odd_authentication_button)
        val progress = findViewById<View>(R.id.odd_authentication_progress)

        button.visibility = View.GONE
        progress.visibility = View.VISIBLE
    }

    protected fun hideProgress() {
        val button = findViewById<View>(R.id.odd_authentication_button)
        val progress = findViewById<View>(R.id.odd_authentication_progress)

        button.visibility = View.VISIBLE
        progress.visibility = View.GONE
    }

    protected fun showFailure() {
        val failure = findViewById<View>(R.id.odd_authentication_failure_container)
        failure.visibility = View.VISIBLE
    }

    protected fun hideFailure() {
        val failure = findViewById<View>(R.id.odd_authentication_failure_container)
        failure.visibility = View.GONE
    }

    protected fun setFailureMessage(message: String) {
        val failureMessage = findViewById<TextView>(R.id.odd_authentication_failure_message)
        failureMessage.text = message
    }

    protected fun finishLogin(accountType: String, viewer: OddViewer) {
        val account = Account(viewer.id, accountType)
        if (intent.getBooleanExtra(ARG_IS_ADDING_NEW_ACCOUNT, false)) {
            Log.d(TAG, "finishLogin > addAccountExplicitly - $authTokenType -  ${viewer.id}")
            Log.d(TAG, "${account.name} - $authTokenType - ${viewer.jwt}")

            accountManager.addAccountExplicitly(account, viewer.jwt, null)
            accountManager.setAuthToken(account, authTokenType, viewer.jwt)
        } else {
            Log.d(TAG, "finishLogin > setPassword - $authTokenType -  ${viewer.id}")
            Log.d(TAG, "${account.name} - $authTokenType - ${viewer.jwt}")
            accountManager.setAuthToken(account, authTokenType, viewer.jwt)
            accountManager.setPassword(account, viewer.jwt)
        }

        val data = Bundle()
        val result = Intent()
        result.putExtras(data)
        setAccountAuthenticatorResult(data)
        setResult(RESULT_OK, result)
        finish()
    }

    companion object {
        private val TAG = OddAuthenticationActivity::class.java.simpleName
        val ARG_ACCOUNT_TYPE = "${OddAuthenticationActivity::class.java.name}.ARG_ACCOUNT_TYPE"
        val ARG_AUTH_TYPE = "${OddAuthenticationActivity::class.java.name}.ARG_AUTH_TYPE"
        val ARG_ACCOUNT_EMAIL = "${OddAuthenticationActivity::class.java.name}.ARG_ACCOUNT_EMAIL"
        val ARG_IS_ADDING_NEW_ACCOUNT = "${OddAuthenticationActivity::class.java.name}.ARG_IS_ADDING_NEW_ACCOUNT"
    }
}